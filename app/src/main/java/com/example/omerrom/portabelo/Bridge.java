package com.example.omerrom.portabelo;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Tomer on 28/07/2016.
 */
public class Bridge extends Activity{

    private static int frat;
    private float pressure;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public float getPressure() {
        return pressure;
    }

    public static int getFrat() {
        return frat;
    }
}
