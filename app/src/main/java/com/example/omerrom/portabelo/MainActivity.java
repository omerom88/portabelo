package com.example.omerrom.portabelo;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

public class MainActivity extends Activity {

    private static final int[] BUTTONS = {R.id.E_LOW, R.id.A, R.id.D, R.id.G, R.id.B, R.id.E_HIGH};
    private static CordManager cordManager;

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //////////////// the gesture  /////////////////
        cordManager = CordManager.getInstance(getApplicationContext());
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        GestureListener.setHeight(size.y);
        /////////////// string buttons  ///////////////
        for (int i = 0; i < BUTTONS.length; i++) {
            LinearLayout stringButton = (LinearLayout) findViewById(BUTTONS[i]);
            assert stringButton != null;
            stringButton.setTag(i);
            stringButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent event) {
//                    long startTime = System.currentTimeMillis();
                    int index = (int) view.getTag();
                    cordManager.cancelTask(index);
                    cordManager.runTask(index, event);
//                    Log.e("startTime: ", "" + (System.currentTimeMillis() - startTime));
                    return true;
                }
            });
        }
    }

    /**
     * Called when another activity is taking focus.
     */
    @Override
    protected void onPause() {
        super.onPause();
        cordManager.cancelAllTasks();
        Log.d("", "The onPause() event");
    }

    /**
     * Called when the activity is no longer visible.
     */
    @Override
    protected void onStop() {
        super.onStop();
        Log.d("", "The onStop() event");
    }

    /**
     * Called just before the activity is destroyed.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("", "The onDestroy() event");
    }
}
